;;;; track-analysis.lisp

;;; Functions

(defun get-switches (pieces)
  (loop for piece across pieces
        for i from 0
        when (getf piece :switch) collect (append piece (list :index i))))


(defun lane-path-to-pieces (pieces lane-path)
  (loop with lane = (pop lane-path)
        for piece across pieces
        for i from 0
        for lst = (copy-list piece)
        for upcoming-switch = (getf (nth-piece pieces (1+ i)) :switch)
        for next-lane = (when upcoming-switch
                          (pop lane-path))
        do (setf lst (append lst (list :lane lane :index i)))
           (when (and next-lane
                      (/= next-lane lane))
             (setf lst  (append lst (list :switch-to next-lane))
                   lane  next-lane))
        collect lst))


(defun lane-length (piece lane)
  (let* ((angle (getf piece :angle))
         (radius (getf piece :radius))
         (dfc (if (and angle (< angle 0))
                  (* -1 (getf lane :distance-from-center))
                  (getf lane :distance-from-center))))
    (if (and angle radius)
        (* 2pi (- radius dfc) (/ (abs angle) 360))
        (getf piece :length))))


;; A car can only switch one lane per switch. So it is not possible to switch
;; from track 1 to track 3.
(defun possible-routes (n-lanes n-switches &optional (lane 0) (route nil))
  ;(declare (optimize (debug 0) (safety 0) (speed 3))
  ;         (type fixnum n-lanes n-switches lane)
  ;         (type list route)
  ;         (inline - <= abs append append1 list))
  (when (<= n-switches 0)
    (return-from possible-routes (list (append1 route lane))))
  (let ((next-lanes (loop for next-lane from 0 below n-lanes
                          when (<= (abs (- next-lane lane)) 1)
                            collect next-lane))
        (routes nil))
    (loop for next-lane in next-lanes
          for results = (possible-routes n-lanes (- n-switches 1) next-lane
                                         (append1 route lane))
          do (setf routes (append routes results)))
    routes))


(defun route-length (pieces lanes route)
  (loop with lane = (aref lanes (pop route))
        with total-length = 0
        for piece across pieces
        for switch = (getf piece :switch)
        do (when switch
             (setf lane (aref lanes (pop route))))
           (incf total-length (lane-length piece lane))
        finally (return total-length)))


(defun count-lane-switches (lane-path)
  (loop with prev-lane = (pop lane-path)
        for lane in lane-path
        when (/= lane prev-lane) sum 1
        do (setf prev-lane lane)))


(defun sort-routes-by-length (pieces lanes routes)
  (sort (loop for route in routes
              collect (list :route route
                            :length (route-length pieces lanes route)))
        (lambda (a b)
          (if (= (getf a :length) (getf b :length))
              ;; if lengths are equal minimize lane switches
              (< (count-lane-switches (getf a :route))
                 (count-lane-switches (getf b :route)))
              (< (getf a :length) (getf b :length))))))


(defun best-path (pieces analyzed-track lane &optional (piece 0))
  "Return the best path from ANALYZED-TRACK for current LANE and optionally
  from current PIECE."
  (if (= piece 0)
      (getf (elt (elt analyzed-track lane) 0) :route)
      (loop for i-path from 0
            do (loop for at-lane in analyzed-track
                     for route = (getf (nth i-path at-lane) :route)
                     for at-pieces = (lane-path-to-pieces pieces route)
                     do (when (= lane (getf (elt at-pieces piece) :lane))
                          (return-from best-path route))))))


(defun precalculate-laps (pieces analyzed-track laps piece lane)
  "Precalculates the lanes to take for each piece up to LAPS in the future
  (generally only the first lap differs from the others) and from the current
  PIECE and LANE."
  (let* ((lane-path (best-path pieces analyzed-track lane piece))
         (path (subseq (lane-path-to-pieces pieces lane-path) piece)))
    (loop for i from 1 below laps
          for lap-end-lane = (last1 lane-path)
          for next-lane-path = (best-path pieces analyzed-track lap-end-lane)
          for next-path = (lane-path-to-pieces pieces next-lane-path)
          do (setf path (append path next-path)))
    path))


(defun analyse-track (pieces lanes)
  ;(declare (optimize (debug 0) (safety 0) (speed 3))
  ;         (type fixnum n-lanes n-switches))
  (let* ((n-lanes (length lanes))
         (n-switches (length (get-switches pieces)))
         (routes (loop for lane from 0 below n-lanes
                       for routes = (possible-routes n-lanes n-switches lane)
                       collect (sort-routes-by-length pieces lanes routes))))
    (values routes
            (loop for lst in routes sum (length lst)))))
