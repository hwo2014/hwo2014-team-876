;;;; main.lisp

(setf sb-impl::*default-external-format* :utf-8)

;;; Packages

(require :asdf)
(require :sb-posix)

;; Silence these so my own warnings & errors do not drown out.
(let ((*error-output* (make-broadcast-stream)))
  (load "3rd-party/cl-store/cl-store.asd")
  (load "3rd-party/jsown/jsown.asd")
  (load "3rd-party/usocket/usocket.asd"))

(asdf:oos 'asdf:load-op :cl-store)
(asdf:oos 'asdf:load-op :jsown)
(asdf:oos 'asdf:load-op :usocket)


;;; Other Files

(load "globals.lisp")
(load "common.lisp")
(load "conditions-and-handlers.lisp")
(load "msg-sending.lisp")
(load "msg-parsing.lisp")
(load "track-analysis.lisp")
(load "main-loop.lisp")


;;; Functions

(defun check-input-parameters (host port bot-name)
  (cond ((not (and host port))
         (format t "Usage: hwo2014-clbot HOST PORT [BOT-NAME]~%")
         (exit))
        ((null bot-name)
         (format t "Error: No bot name found, please set the BOTNAME ~
                    environment variable or supply the name on the ~
                    commandline.~%")
         (exit))
        ((null *bot-key*)
         (dbgmsg "[Warning] No bot key found, you might have trouble ~
                  connecting to the official servers!~%"))))


(defun abort-if-not (msg msg-type)
  (unless (msg-type-p msg msg-type)
    (dbgmsg "Received incorrect message, aborting...~%~S~%" msg)
    (exit)))


(defun game-init (socket stream)
  (dbgmsg "Waiting for GAME-INIT or TOURNAMENT-END...~%")
  (let ((msg (wait-for-input-and-parse-msg socket stream))
        game-init car-positions game-start analyzed-track)
    (if (msg-type-p msg :tournament-end)
        (return-from game-init :tournament-end)
        (abort-if-not msg :game-init))
    (setf game-init      msg
          analyzed-track (analyse-track (getf game-init :pieces)
                                        (getf game-init :lanes)))
    (dbgmsg "---~%Waiting for CAR-POSITIONS...~%")
    (setf msg (wait-for-input-and-parse-msg socket stream))
    (abort-if-not msg :car-positions)
    (setf car-positions msg)
    (dbgmsg "---~%Waiting for GAME-START...~%")
    (setf msg (wait-for-input-and-parse-msg socket stream))
    (abort-if-not msg :game-start)
    (setf game-start msg)
    (list :game-init game-init :car-positions car-positions
          :game-start game-start :analyzed-track analyzed-track)))


(defun game-join (socket stream bot-name track)
  ;; Right, so "joinRace" is just for our own custom races.
  (if track
      (msg-join-race stream :name bot-name :key *bot-key* :car-count 1
                     :track track)
      (msg-join stream :name bot-name :key *bot-key*))
  (dbgmsg "Waiting for JOIN or JOIN-RACE...~%")
  (let ((msg (wait-for-input-and-parse-msg socket stream))
        join your-car)
    (abort-if-not msg :join)
    (setf join msg)
    (dbgmsg "---~%Waiting for YOUR-CAR...~%")
    (setf msg (wait-for-input-and-parse-msg socket stream))
    (abort-if-not msg :your-car)
    (setf your-car msg)
    (list :join join :your-car your-car)))


(defun write-track-data (init-data track-data)
  (let ((datetime (multiple-value-bind (sec min hour day mon year)
                      (decode-universal-time (get-universal-time))
                    (format nil "~4,'0D-~2,'0D-~2,'0D-~2,'0D~2,'0D~2,'0D"
                            year mon day hour min sec)))
        (track-id (getf (getf init-data :game-init) :track-id)))
    (dbgmsg "Writing track data to ~A...~%"
            (mkstr track-id "-" datetime ".cl-store"))
    (cl-store:store track-data (mkstr "race-data/" track-id "-" datetime
                                      ".cl-store"))))


;;; Main Program

(defun main (&key (host (second *posix-argv*)) (port (third *posix-argv*))
                  (bot-name (if (fourth *posix-argv*)
                                (fourth *posix-argv*)
                                *bot-name*))
                  ;(track (fifth *posix-argv*)) (ga-piece-data nil))
                  (track nil) (ga-piece-data nil))
  (check-input-parameters host port bot-name)
  (let (socket stream join-data init-data track-data ga-result)
    (dbgmsg "=== bot start ===~%")
    (dbgmsg "Connecting to server at ~A:~A...~%" host port)
    (handler-case
        (progn
          (setf socket (usocket:socket-connect host (parse-integer port)))
          (dbgmsg "Connected.~%")
          (setf stream (usocket:socket-stream socket))
          (dbgmsg "--- game join ---~%")
          (setf join-data (game-join socket stream bot-name track))
          (loop with tournament-end = nil
                until tournament-end
                do (dbgmsg "--- game init ---~%")  ; aborts on TOURNAMENT-END
                   (setf init-data (game-init socket stream))
                   (if (equal init-data :tournament-end)
                       (setf tournament-end t)
                       (progn
                         (dbgmsg "--- main loop ---~%")
                         (let ((results (main-loop socket stream join-data
                                                   init-data ga-piece-data)))
                           (setf ga-result      (getf results :ga-result)
                                 track-data     (getf results :track-data))
                           (when (equal ga-result :crash)
                             (loop-finish)))
                         (dbgmsg "--- game end ---~%")
                         (set-racing-state)
                         (print-track-data track-data)
                         (when (or (string= "DELL-XPS-17" (machine-instance))
                                   (string= "dlf-winkels" (machine-instance)))
                                   ;(string= "ip-10-39-37-122" (machine-instance)))
                           (write-track-data init-data track-data)))))
          (dbgmsg "Done. Closing socket...~%")
          (usocket:socket-close socket))
      #+sbcl (sb-int:simple-stream-error () (connection-lost))
      #+sbcl (sb-sys:interactive-interrupt () (user-interrupt))
      (usocket:connection-refused-error () (connection-refused))
      (server-connection-closed-error () (connection-lost)))
    (if ga-piece-data
        (progn (format t "GA result: ~S~%" ga-result)
               ga-result)
        (exit))))
