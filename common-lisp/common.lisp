;;;; common.lisp

;;; Logging Functions

(defun dbgmsg (&rest args)
  (when *verbose*
    (let ((*print-pretty* nil))
      (apply #'format (append (list *standard-output*) args)))
    (force-output *standard-output*)))


(defun errmsg (&rest args)
  (apply #'format (append (list *error-output*) args))
  (force-output *error-output*))


;;; Functions

(defun append1 (list item)
  (append list (list item)))


(defun last1 (sequence)
  (let ((length (length sequence)))
    (when (> length 0)
      (elt sequence (- length 1)))))


(defun mkstr (&rest args)
  (with-output-to-string (s)
    (dolist (a args) (princ a s))))


(defun nth-piece (pieces n)
  (aref pieces (mod n (length pieces))))
