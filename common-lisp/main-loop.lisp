;;;; main-loop.lisp

;;; Functions

(defun get-my-angle (msg color &key (absolute nil))
  (loop for car in (getf msg :cars)
        do (when (equal color (getf car :color))
             (if absolute
                 (return-from get-my-angle (abs (getf car :angle)))
                 (return-from get-my-angle (getf car :angle))))))


;; This refers to the distance travelled each tick, not total distance.
(defun get-my-distance (pieces lanes piece in-piece-distance prev-piece
                        prev-in-piece-distance prev-lane)
  (if (= piece prev-piece)
      (- in-piece-distance prev-in-piece-distance)
      (let ((prev-lane-length (lane-length (nth-piece pieces prev-piece)
                                           (elt lanes prev-lane))))
        (+ (- prev-lane-length prev-in-piece-distance) in-piece-distance))))


(defun get-my-in-piece-distance (msg color)
  (loop for car in (getf msg :cars)
        do (when (equal color (getf car :color))
             (return-from get-my-in-piece-distance
                          (getf car :in-piece-distance)))))


(defun get-my-lane (msg color)
  (loop for car in (getf msg :cars)
        do (when (equal color (getf car :color))
             (return-from get-my-lane (getf car :start-lane-index)))))


(defun get-my-lap (msg color)
  (loop for car in (getf msg :cars)
        do (when (equal color (getf car :color))
             (return-from get-my-lap (getf car :lap)))))


(defun get-my-piece (msg color)
  (loop for car in (getf msg :cars)
        do (when (equal color (getf car :color))
             (return-from get-my-piece (getf car :piece-index)))))


(defun print-track-data (track-data)
  (dbgmsg "Track data:~%")
  (dbgmsg "     n |    angle |    accel |    speed | throttle | turbo~%~
           -------+----------+----------+----------+----------+-------~%")
  (loop for plst across track-data
        for i from 0
        do (dbgmsg " ~5D | ~8,3F | ~8,3F | ~8,3F | ~8,3F |   ~A~%"
                   i (getf plst :angle) (getf plst :acceleration)
                   (getf plst :speed) (getf plst :throttle)
                   (getf plst :turbo))))


;; Ugh, this isn't pretty...
(let (acceleration analyzed-track angle distance game-tick in-piece-distance
      lane lap laps my-color path path-piece piece prev-acceleration prev-angle
      prev-distance prev-in-piece-distance prev-lane prev-piece prev-speed
      prev-throttle prev-total-distance speed starting-lane state throttle
      total-distance track)


  (defun get-my-best-lap (msg)
    (loop for best-lap in (getf msg :best-laps)
          do (when (equal my-color (getf best-lap :color))
               (return-from get-my-best-lap (getf best-lap :millis)))))


  (defun get-my-color ()
    my-color)


  (defun init-car-positions-vars (join-data init-data pieces)
    (setf my-color (getf (getf join-data :your-car) :color)
          starting-lane (get-my-lane (getf init-data :car-positions) my-color)
          analyzed-track (getf init-data :analyzed-track)
          game-tick 0
          lane starting-lane
          lap 0
          laps (if (getf (getf init-data :game-init) :laps)
                   (getf (getf init-data :game-init) :laps)
                   64)  ;; FIXME hack to make qualifying rounds work
          path (precalculate-laps pieces analyzed-track laps 0 lane)
          path-piece (pop path)
          state :ramp-up-throttle
          track (getf (getf init-data :game-init) :track-id)
          ;; race data
          angle 0
          prev-angle 0
          acceleration 0
          prev-acceleration 0
          distance 0
          prev-distance 0
          in-piece-distance 0
          prev-lane lane
          prev-in-piece-distance 0
          piece 0
          prev-piece 0
          speed 0
          prev-speed 0
          throttle 0.1
          prev-throttle 0.0
          total-distance 0
          prev-total-distance 0))


  (defun handle-car-positions (stream msg pieces lanes piece-data track-data
                               turbo turbo-on &optional (ga-piece-data))
    (declare (ignore ga-piece-data))
    (dbgmsg "state: ~S~%" state)
    (setf game-tick (getf msg :game-tick)
          lane      (get-my-lane msg my-color)
          lap       (get-my-lap msg my-color))
    (let ((speed (getf (aref (aref piece-data prev-piece) prev-lane)
                       :max-speed)))
      (when (or (not speed)
                (> prev-speed speed))
        (setf (getf (aref (aref piece-data prev-piece) prev-lane) :max-speed)
              prev-speed)))
    (update-tick-values msg pieces lanes)           ; T e e c n b   e g d
    (update-track-data track-data turbo-on pieces)  ;  h s   a   e m r e
    ;; START: car AI
    (cond ((and (equal state :ramp-up-throttle)
                (<= acceleration 0.1)
                (>= throttle 1.0))
           (setf state :ramp-down-throttle)
           (decf throttle 0.1))
          ((and (equal state :ramp-up-throttle)
                (<= acceleration 0.1))
           (incf throttle 0.1))
          ((equal state :ramp-up-throttle))
           ; do nothing
          ((and (equal state :ramp-down-throttle)
                (<= acceleration 0.1)
                (<= throttle 0.1))
           (setf state :qualifiers)
           (setf throttle 1.0))
          ((and (equal state :ramp-down-throttle)
                (<= acceleration 0.1))
           (decf throttle 0.1))
          ((equal state :ramp-down-throttle))
           ; do nothing
          (t
           (setf throttle (target-throttle-this-tick pieces lanes piece-data
                                                     track-data))))
    (let ((switch (switch-track pieces)))
      (cond (switch
             (msg-switch-lane stream lane switch game-tick))
            ((and turbo (= piece turbo))
             (progn (msg-turbo stream game-tick)
                    (setf turbo nil)))
            (t
             (msg-throttle stream throttle game-tick))))
    ;;   END: car AI
    ;(print-track-data track-data)
    (update-prev-values)
    turbo)


  (defun handle-crash (pieces piece-data)
    (dbgmsg "Crash: prev-speed=~,2F  prev-angle=~,2F~%" prev-speed prev-angle)
    (let* ((crash-piece (aref pieces prev-piece))
           (angle (getf crash-piece :angle))
           (radius (getf crash-piece :radius)))
      (dbgmsg "Crash piece: ~S~%" crash-piece)
      (loop for i from 0 below (length pieces)
            for icp = (mod (+ piece i) (length pieces))
            for check-piece = (aref pieces icp)
            for check-angle = (getf check-piece :angle)
            for check-radius = (getf check-piece :radius)
            do (when (and angle radius check-angle check-radius
                          (= (abs check-angle) (abs angle))
                          (= check-radius radius))
                 (dbgmsg "Update ~D:~S with crash info.~%" icp check-piece)
                 (when (or (not (getf (aref (aref piece-data icp) prev-lane)
                                      :max-speed))
                           (< prev-speed
                              (getf (aref (aref piece-data icp) prev-lane)
                                    :max-speed)))
                   (setf (getf (aref (aref piece-data icp) prev-lane)
                               :max-speed)
                         prev-speed))
                 (setf (getf (aref (aref piece-data icp) prev-lane)
                             :crash-limit)
                       t)))))


  ;; Best turbo piece is defined as the nearest sequences of straight pieces,
  ;; we want to use the turbo as quickly as possible.  However, and these are
  ;; untested assumptions, since the turboFactor is probably a multiplier it
  ;; is better to use it at the highest speed possible so it might be an idea
  ;; to not pick the first straight piece.
  (defun find-best-turbo-piece (pieces)
    (loop with straights = nil
          with straight = nil
          with len = (length pieces)
          with best-piece = (mod (1+ piece) len)
          for i from 1 below (if (>= (1+ lap) laps)
                                 (- len (1+ piece))  ; remains of last lap
                                 len)
          do (if (getf (aref pieces (mod (+ piece i) len)) :length)
                 (push (mod (+ piece i) len) straight)
                 (when straight
                   (push (nreverse straight) straights)
                   (setf straight nil)))
          finally (setf straights (sort straights
                                        (lambda (a b)
                                          (> (length a) (length b)))))
                  (when straights
                    (if (> (length (first straights)) 3)
                        (setf best-piece (second (first straights)))
                        (setf best-piece (first (first straights)))))
                  (return best-piece)))


  (defun set-racing-state ()
    (setf state :racing))


  (defun switch-track (pieces)
    (when (and (getf path-piece :index)
               (/= piece (getf path-piece :index)))
      (dbgmsg ">>> new piece: ~S => " path-piece)
      (setf path-piece (pop path))
      (dbgmsg "~S~%" path-piece)
      (when (and (getf path-piece :lane)
                 (not (getf path-piece :switch))  ; switches confuse this check
                 (/= lane (getf path-piece :lane)))
        (dbgmsg "We're on the wrong lane, calculating new path... ")
        (setf path       (precalculate-laps pieces analyzed-track (- laps lap)
                                            piece lane)
              path-piece (pop path))
        (dbgmsg "new piece: ~S~%" path-piece))
      (getf path-piece :switch-to)))


  (defun dist-to-piece (pieces lanes from-piece to-piece)
    (cond ((= from-piece to-piece) (return-from dist-to-piece 0))
          ((< to-piece from-piece) (incf to-piece (length pieces))))
    (loop with dist = (- (lane-length (aref pieces from-piece)
                                      (aref lanes lane))
                         in-piece-distance)
          for i from 1 below (- to-piece from-piece)
          do (incf dist (lane-length (aref pieces (+ from-piece i))
                                     (aref lanes lane)))
          finally (return dist)))


  ;; Looks ahead 8 pieces to check speeds. It should actually determine by
  ;; current speed and max. braking power how far to look ahead.
  (defun target-throttle-this-tick (pieces lanes piece-data track-data)
    (loop with look-ahead = 3
          with prev-apt = nil
          with target-throttle = 1.0
          for i from 0 below look-ahead
          for ip = (mod (+ piece i) (length pieces))
          for current-piece = (aref pieces ip)
          for data = (aref (aref piece-data ip) lane)
          for dist = (dist-to-piece pieces lanes piece ip)
          for max-speed = (getf data :max-speed)
          for apt = (when max-speed
                      (if (= dist 0)
                          (- max-speed speed)
                          (/ (- max-speed speed) dist)))
          do (dbgmsg "dist from ~D to ~D: ~S (~S)~%" piece ip dist apt)
             (if (null apt)
                 (setf target-throttle 1.0)
                 (when (or (null prev-apt)
                           (< apt prev-apt))
                   (let* (;; oh man, this code's going public sometime
                          (tmp1 (loop for td across track-data
                                      when (< (getf td :acceleration) apt)
                                        collect td))
                          (tmp2 (loop for td in (sort tmp1
                                                      (lambda (a b)
                                                        (< (getf a :speed)
                                                           (getf b :speed))))
                                      when (< speed (getf td :speed))
                                        collect td)))
                     (if tmp2
                         (setf target-throttle (getf (first tmp2) :throttle))
                         (setf target-throttle throttle)))))
          finally (return target-throttle)))


  (defun update-prev-values ()
    (setf prev-angle             angle
          prev-acceleration      acceleration
          prev-distance          distance
          prev-in-piece-distance in-piece-distance
          prev-lane              lane
          prev-piece             piece
          prev-speed             speed
          prev-throttle          throttle  ; this is BS actually
          prev-total-distance    total-distance))


  (defun update-tick-values (msg pieces lanes)
    (setf angle             (get-my-angle msg my-color :absolute t)
          piece             (get-my-piece msg my-color)
          in-piece-distance (get-my-in-piece-distance msg my-color)
          distance          (get-my-distance pieces lanes piece
                                             in-piece-distance prev-piece
                                             prev-in-piece-distance prev-lane)
          total-distance    (+ total-distance distance)
          speed             (- total-distance prev-total-distance)
          acceleration      (- speed prev-speed)))


  ;; FIXME should add turbo column!
  (defun update-track-data (track-data turbo-on pieces)
    (let ((new-plst (list :angle angle
                          :acceleration acceleration
                          :speed prev-speed
                          :throttle throttle
                          :turbo turbo-on)))
      (when (and ;; only collect from straight pieces
                 (getf (aref pieces piece) :length)
                 (getf (aref pieces prev-piece) :length)
                 (> acceleration 0)
                 (not (loop with result = nil
                            for plst across track-data
                            do (when (string= (format nil "~,3F" acceleration)
                                              (format nil "~,3F"
                                                    (getf plst :acceleration)))
                               (setf result t)
                               (loop-finish))
                            finally (return result))))
        (vector-push-extend new-plst track-data)
        (setf track-data (sort track-data (lambda (a b)
                                            (< (getf a :acceleration)
                                               (getf b :acceleration)))))))))


;;; Main Loop Function

(defun main-loop (socket stream join-data init-data &optional (ga-piece-data))
  (msg-throttle stream 1.0 0)
  (init-car-positions-vars join-data init-data
                           (getf (getf init-data :game-init) :pieces))
  (loop with ga-result = nil
        with pieces = (getf (getf init-data :game-init) :pieces)
        with lanes = (getf (getf init-data :game-init) :lanes)
        with piece-data = (let ((array (make-array (length pieces))))
                            (loop for i from 0 below (length array)
                                  do (setf (aref array i)
                                           (make-array (length lanes)
                                                       :initial-element nil)))
                            array)
        with track-data = (make-array 0 :adjustable t :fill-pointer 0)
        with turbo = nil
        with turbo-on = nil
        for msg = (wait-for-input-and-parse-msg socket stream)
        do (cond ((msg-type-p msg :car-positions)
                  (setf turbo (handle-car-positions stream msg pieces lanes
                                   piece-data track-data turbo turbo-on
                                   ga-piece-data)))
                 ((msg-type-p msg :crash)
                  (handle-crash pieces piece-data)
                  (when ga-piece-data
                    (setf ga-result :crash)
                    (loop-finish)))
                 ((msg-type-p msg :lap-finished)
                  (loop for piece across piece-data
                        for i from 0
                        do (dbgmsg "~3D: ~S~%" i piece)))
                 ((msg-type-p msg :game-end)
                  (when ga-piece-data
                    (setf ga-result (get-my-best-lap msg)))
                  (loop-finish))
                 ((msg-type-p msg :turbo-available)
                  (setf turbo (find-best-turbo-piece pieces))
                  (dbgmsg "Best turbo piece: ~A~%" turbo))
                 ((and (msg-type-p msg :turbo-end)
                       (string= (getf msg :color) (get-my-color)))
                  (setf turbo-on nil))
                 ((and (msg-type-p msg :turbo-start)
                       (string= (getf msg :color) (get-my-color)))
                  (setf turbo-on t)))
           ;(print-track-data track-data)
           (dbgmsg "---~%")
        finally (return (list :ga-result ga-result :track-data track-data))))
