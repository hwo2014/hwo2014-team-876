;;;; msg-sending.lisp

;;; Functions

(defun msg-send (stream json)
  (dbgmsg "Sending: ~A~%" json)
  (format stream "~A~%" json)
  (finish-output stream))


(defun msg-join (stream &key (name *bot-name*) (key *bot-key*))
  (let ((name (if (>= (length name) 16)  ; max name length is 16 chars
                  (subseq name 0 16)
                  name)))
    (msg-send stream
              (jsown:to-json `(:obj ("msgType" . "join")
                                    ("data"    . (:obj ("name" . ,name)
                                                       ("key"  . ,key))))))))


(defun msg-join-race (stream &key (name *bot-name*) (key *bot-key*)
                                  (car-count 1) (track nil) (password nil))
  (let* ((name (if (>= (length name) 16)  ; max name length is 16 chars
                   (subseq name 0 16)
                   name))
         (data `(:obj ("botId"    . (:obj ("name" . ,name)
                                          ("key"  . ,key)))
                      ("carCount" . ,car-count))))
    (when track
      (setf data (append1 data (cons "trackName" track))))
    (when password
      (setf data (append1 data (cons "password" password))))
    (msg-send stream
              (jsown:to-json `(:obj ("msgType" . "joinRace")
                                    ("data"    . ,data))))))


(defun msg-ping (stream &optional game-tick)
  (let ((msg `(:obj ("msgType" . "ping"))))
    (when game-tick
      (setf msg (append1 msg (cons "gameTick" game-tick))))
    (msg-send stream (jsown:to-json msg))))


;; FIXME currently this function assumes the lanes are ordered from left to
;;       right
(defun msg-switch-lane (stream from-lane to-lane &optional game-tick)
  (let* ((direction (cond ((< from-lane to-lane) "Right")
                          ((> from-lane to-lane) "Left")
                          (t (errmsg "[Error] from-lane and to-lane equal")
                             (msg-ping stream game-tick)
                             (return-from msg-switch-lane))))
         (msg `(:obj ("msgType"  . "switchLane")
                     ("data"     . ,direction))))
    (when game-tick
      (setf msg (append1 msg (cons "gameTick" game-tick))))
    (msg-send stream (jsown:to-json msg))))


(defun msg-throttle (stream value &optional game-tick)
  (let* ((value (cond ((< value 0)
                       (dbgmsg "[Warning] throttle < 0: ~S~%" value)
                       0.0)
                      ((> value 1)
                       (dbgmsg "[Warning] throttle > 1: ~S~%" value)
                       1.0)
                      (t
                       value)))
         (msg  `(:obj ("msgType"  . "throttle")
                      ("data"     . ,value))))
    (when game-tick
      (setf msg (append1 msg (cons "gameTick" game-tick))))
    (msg-send stream (jsown:to-json msg))))


(defun msg-turbo (stream &optional game-tick)
  (let ((msg `(:obj ("msgType"  . "turbo")
                    ("data"     . "Lithp or Die!"))))
    (when game-tick
      (setf msg (append1 msg (cons "gameTick" game-tick))))
    (msg-send stream (jsown:to-json msg))))
