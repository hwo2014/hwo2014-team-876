;;;; conditions-and-handlers.lisp

;;; Conditions

(define-condition server-connection-closed-error (error)
  nil)


;;; Handlers

(defun connection-lost ()
  (dbgmsg "Connection lost. Aborting...~%")
  (if *running-in-ga*
      nil
      (exit :code 104)))


(defun connection-refused ()
  (dbgmsg "Connection refused. Aborting...~%")
  (if *running-in-ga*
      nil
      (exit :code 111)))


(defun user-interrupt ()
  (dbgmsg "User interrupt. Aborting...~%")
  (if *running-in-ga*
      nil
      (exit)))
