;;;; msg-parsing.lisp

;;; Functions

;; Most vals are hidden in the data object so we try that transparently.
(defun json-val (json value)
  (when value
    (let ((val (ignore-errors (jsown:val json value))))
      (if val
          val
          (let ((data (ignore-errors (jsown:val json "data"))))
            (when data
              (ignore-errors (jsown:val data value))))))))


(defun json-type-p (json msg-type)
  (string= (string-downcase (json-val json "msgType"))
           (string-downcase msg-type)))


(defun msg-type-p (msg msg-type)
  (equal msg-type (getf msg :type)))


;;; Parse Functions

(defun parse-car (json)
  (list :name (json-val (json-val json "id") "name")
        :color (json-val (json-val json "id") "color")
        :angle (json-val json "angle")
        :piece-index (json-val (json-val json "piecePosition") "pieceIndex")
        :in-piece-distance (json-val (json-val json "piecePosition")
                                     "inPieceDistance")
        :start-lane-index (json-val (json-val (json-val json "piecePosition")
                                              "lane")
                                    "startLaneIndex")
        :end-lane-index (json-val (json-val (json-val json "piecePosition")
                                            "lane")
                                  "endLaneIndex")
        :lap (json-val (json-val json "piecePosition") "lap")))


(defun parse-car-init (json)
  (list :name (json-val (json-val json "id") "name")
        :color (json-val (json-val json "id") "color")
        :length (json-val (json-val json "dimensions") "length")
        :width (json-val (json-val json "dimensions") "width")
        :guide-flag-position (json-val (json-val json "dimensions")
                                       "guideFlagPosition")))


(defun parse-car-positions (json)
  (list :type :car-positions
        :cars (loop for car in (json-val json "data") collect (parse-car car))
        :game-id (json-val json "gameId")
        :game-tick (json-val json "gameTick")))


(defun parse-crash (json)
  (list :type :crash
        :name (json-val json "name")
        :color (json-val json "color")
        :game-id (json-val json "gameId")
        :game-tick (json-val json "gameTick")))


(defun parse-dnf (json)
  (list :type :dnf
        :name (json-val (json-val json "car") "name")
        :color (json-val (json-val json "car") "color")
        :reason (json-val json "reason")
        :game-id (json-val json "gameId")
        :game-tick (json-val json "gameTick")))


(defun parse-finish (json)
  (list :type :finish
        :name (json-val json "name")
        :color (json-val json "color")
        :game-id (json-val json "gameId")))


(defun parse-game-end (json)
  (list :type :game-end
        :results
        (loop for result in (json-val json "results")
              collect (list :name (json-val (json-val result "car") "name")
                            :color (json-val (json-val result "car") "color")
                            :laps (json-val (json-val result "result") "laps")
                            :ticks (json-val (json-val result "result")
                                             "ticks")
                            :millis (json-val (json-val result "result")
                                              "millis")))
        :best-laps
        (loop for bl in (json-val json "bestLaps")
              collect (list :name (json-val (json-val bl "car") "name")
                            :color (json-val (json-val bl "car") "color")
                            :lap (json-val (json-val bl "result") "lap")
                            :ticks (json-val (json-val bl "result") "ticks")
                            :millis (json-val (json-val bl "result")
                                              "millis")))
        :game-id (json-val json "gameId")))


(defun parse-lane (json)
  (list :distance-from-center (json-val json "distanceFromCenter")
        :index (json-val json "index")))


(defun parse-piece (json)
  (loop for item in json
        when (consp item)
          append (list (intern (string-upcase (car item)) :keyword)
                       (cdr item))))


(defun parse-race (json)
  (list :track-id (json-val (json-val json "track") "id")
        :track-name (json-val (json-val json "track") "name")
        :pieces (loop for piece in (json-val (json-val json "track") "pieces")
                      collect (parse-piece piece) into pieces
                      finally (return (coerce pieces 'vector)))
        :lanes (loop for lane in (json-val (json-val json "track") "lanes")
                     collect (parse-lane lane) into lanes
                     finally (return (coerce lanes 'vector)))
        :start-x (json-val (json-val (json-val (json-val json "track")
                                               "startingPoint")
                                     "position")
                           "x")
        :start-y (json-val (json-val (json-val (json-val json "track")
                                               "startingPoint")
                                     "position")
                           "y")
        :start-angle (json-val (json-val (json-val json "track")
                                         "startingPoint")
                               "angle")
        :cars (loop for car in (json-val json "cars")
                    collect (parse-car-init car))
        ;; "laps" is not available in qualifying rounds, hence NIL
        :laps (json-val (json-val json "raceSession") "laps")
        :max-lap-time-ms (json-val (json-val json "raceSession")
                                   "maxLapTimeMs")
        ;; "quickRace" is not available in qualifying rounds, hence NIL
        :quickrace (json-val (json-val json "raceSession") "quickRace")))


(defun parse-game-init (json)
  (append (list :type :game-init)
          (parse-race (json-val json "race"))
          (list :game-id (json-val json "gameId"))))


(defun parse-game-start (json)
  (list :type :game-start
        :game-id (json-val json "gameId")
        :game-tick (json-val json "gameTick")))


(defun parse-join (json)
  (list :type :join
        :name (json-val json "name")
        :key (json-val json "key")))


(defun parse-join-race (json)
  (list :type :join
        :name (json-val (json-val json "botId") "name")
        :key (json-val (json-val json "botId") "key")
        :car-count (json-val json "carCount")
        :trackName (json-val json "trackName")
        :password (json-val json "password")))


(defun parse-lap-finished (json)
  (list :type :lap-finished
        :name (json-val (json-val json "car") "name")
        :color (json-val (json-val json "car") "color")
        :lap (json-val (json-val json "lapTime") "lap")
        :lap-ticks (json-val (json-val json "lapTime") "ticks")
        :lap-millis (json-val (json-val json "lapTime") "millis")
        :laps (json-val (json-val json "raceTime") "laps")
        :race-ticks (json-val (json-val json "raceTime") "ticks")
        :race-millis (json-val (json-val json "raceTime") "millis")
        :ranking (json-val (json-val json "ranking") "overall")
        :fastest-lap (json-val (json-val json "ranking") "fastestLap")
        :game-id (json-val json "gameId")))


(defun parse-spawn (json)
  (list :type :spawn
        :name (json-val json "name")
        :color (json-val json "color")
        :game-id (json-val json "gameId")
        :game-tick (json-val json "gameTick")))


(defun parse-turbo-available (json)
  (list :type :turbo-available
        :factor (json-val json "turboFactor")
        :ms (json-val json "turboDurationMilliseconds")
        :ticks (json-val json "turboDurationTicks")
        :game-id (json-val json "gameId")
        :game-tick (json-val json "gameTick")))


(defun parse-turbo-end (json)
  (list :type :turbo-end
        :name (json-val json "name")
        :color (json-val json "color")
        :game-id (json-val json "gameId")
        :game-tick (json-val json "gameTick")))


(defun parse-turbo-start (json)
  (list :type :turbo-start
        :name (json-val json "name")
        :color (json-val json "color")
        :game-id (json-val json "gameId")
        :game-tick (json-val json "gameTick")))


(defun parse-tournament-end (json)
  (list :type :tournament-end
        :game-id (json-val json "gameId")))


(defun parse-your-car (json)
  (list :type :your-car
        :name (json-val json "name")
        :color (json-val json "color")
        :game-id (json-val json "gameId")))


(defun report-error (json)
  (errmsg "--- error message ---~%~S~%" json)
  (list :type :error
        :data (json-val json "data")))


(defun report-unknown (json)
  (errmsg "--- unknown message ---~%~S~%" json)
  (list :unknown-msg-type (json-val json "msgType")
        :json json))


(defun parse-msg (json)
  (cond ((json-type-p json "carPositions")   (parse-car-positions json))
        ((json-type-p json "crash")          (parse-crash json))
        ((json-type-p json "dnf")            (parse-dnf json))
        ((json-type-p json "finish")         (parse-finish json))
        ((json-type-p json "gameEnd")        (parse-game-end json))
        ((json-type-p json "gameInit")       (parse-game-init json))
        ((json-type-p json "gameStart")      (parse-game-start json))
        ((json-type-p json "join")           (parse-join json))
        ((json-type-p json "joinRace")       (parse-join-race json))
        ((json-type-p json "lapFinished")    (parse-lap-finished json))
        ((json-type-p json "spawn")          (parse-spawn json))
        ((json-type-p json "tournamentEnd")  (parse-tournament-end json))
        ((json-type-p json "turboAvailable") (parse-turbo-available json))
        ((json-type-p json "turboEnd")       (parse-turbo-end json))
        ((json-type-p json "turboStart")     (parse-turbo-start json))
        ((json-type-p json "yourCar")        (parse-your-car json))
        ((json-type-p json "error")          (report-error json))
        (t                                   (report-unknown json))))


;;; Main Parsing Function

(defun wait-for-input-and-parse-msg (socket stream)
  #-win32 (usocket:wait-for-input socket)
  #+win32 (usocket:wait-for-input socket :timeout 0)
  (let ((line (read-line stream nil))
        msg)
    (when (null line)
      (error 'server-connection-closed-error))
    (setf msg (parse-msg (jsown:parse line)))
    (dbgmsg "~A  =>  ~S~%" line msg)
    msg))
