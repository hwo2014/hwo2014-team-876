;;;; globals.lisp

;;; Globals

(defparameter *bot-key* (sb-posix:getenv "BOTKEY"))
(defparameter *bot-name* (sb-posix:getenv "BOTNAME"))

(defparameter *running-in-ga* nil)

(defparameter *verbose* t)

(defparameter 2pi (* 2 pi))  ; PI doesn't have earmuffs either

(defparameter tmp nil)  ; for development
