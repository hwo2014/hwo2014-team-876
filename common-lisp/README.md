## HWO2014 Common Lisp bot template

Install SBCL using your package manager on Linux or using Homebrew on OSX.

Install Quicklisp using instructions on the site:

    http://www.quicklisp.org/beta/#installation
