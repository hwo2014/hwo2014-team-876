;;;; The MIT License
;;;;
;;;; Copyright (c) 2011 John Svazic, Erik Winkels
;;;;
;;;; Permission is hereby granted, free of charge, to any person
;;;; obtaining a copy of this software and associated documentation
;;;; files (the "Software"), to deal in the Software without
;;;; restriction, including without limitation the rights to use,
;;;; copy, modify, merge, publish, distribute, sublicense, and/or sell
;;;; copies of the Software, and to permit persons to whom the
;;;; Software is furnished to do so, subject to the following
;;;; conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
;;;; OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;; OTHER DEALINGS IN THE SOFTWARE.

;;;; author: Erik Winkels <aerique@xs4all.nl>
;;;;
;;;; See README.md for documentation.

(load "main.lisp")


;;; Generics and Stubs
;;;
;;; These are just to make SBCL shut up and have been added after all coding
;;; had been finished.

(defgeneric evolve (p track))
(defgeneric mate (c1 c2))
(defgeneric mutate (c))
(defgeneric set-fitness (c track))
(defgeneric tournament-selection (p))
(defgeneric write-population (population &optional file))

(defun random-gene (length) (declare (ignore length)))


;;; Globals

(defparameter *tracks* '(("keimola" . (40 2 6.0))
                         ("germany" . (57 2 4.5))
                         ("usa"     . (32 3 9.5))
                         ("france"  . (44 2 4.5))))

(setf *verbose* nil)


;;; Classes

(defclass chromosome ()
  ((gene :reader gene :initarg :gene :initform (random-gene 13))
   (fitness :accessor fitness :initarg :fitness :initform 3600000))
  (:documentation "This class is used to define a chromosome for the genetic algorithm
  simulation.
  This class is essentially nothing more than a container for the details
  of the chromosome, namely the gene and the fitness.
  Calling MATE or MUTATE will result in a new chromosome instance being
  created."))


(defclass population ()
  ((size :reader size :initarg :size :initform 1024)
   (tournament-size :reader tournament-size :initarg :tournament-size
                    :initform 3)
   (crossover :reader crossover :initarg :crossover :initform 0.8)
   (elitism :reader elitism :initarg :elitism :initform 0.1)
   (mutation :reader mutation :initarg :mutation :initform 0.03)
   (chromosomes :accessor chromosomes :initarg :chromosomes :initform nil))
  (:documentation "A class representing a population for a genetic algorithm simulation.
  A population is simply a sorted collection of chromosomes
  (sorted by fitness) that has a convenience method for evolution.  This
  implementation of a population uses a tournament selection algorithm for
  selecting parents for crossover during each generation's evolution.
  Calls to EVOLVE will generate a new collection of chromosome objects."))


;;; Functions

(defun random-gene (track-length lanes max-speed)
  "Generates a random gene of LENGTH."
  (loop repeat track-length
        collect (loop repeat lanes
                      for speed = (let ((s (+ max-speed
                                              (- (random (/ max-speed 4.0))
                                                 (/ max-speed 8.0)))))
                                    (if (< s 0.1) 0.1 s))
                      collect (list :max-speed speed :crash-limit t) into lanes
                      finally (return (coerce lanes 'vector)))
                into result
        finally (return (coerce result 'vector))))


(defun make-chromosome (track-length lanes max-speed &optional (gene nil))
  "Returns an instance of the CHROMOSOME class.  If GENE is nil a gene
  will be created using RANDOM-GENE."
  (let ((new-gene (if gene gene (random-gene track-length lanes max-speed))))
    (make-instance 'chromosome :gene new-gene)))


(defun make-population (track &key (size 128) (crossover 0.8) (elitism 0.1)
                                   (mutation 0.3))
  "Returns an instance of the POPULATION class."
  (let ((chromosomes
         (loop with track-len = (elt (assoc track *tracks* :test #'string=) 1)
               with lanes = (elt (assoc track *tracks* :test #'string=) 2)
               with max-speed = (elt (assoc track *tracks* :test #'string=) 3)
               repeat size
               collect (make-chromosome track-len lanes max-speed))))
    (make-instance 'population :size size :crossover crossover :elitism elitism
                               :mutation mutation :chromosomes chromosomes)))


;;; Chromosome Methods

(defmethod set-fitness ((c chromosome) track)
  "Sets fitness by racing CHROMOSOME on TRACK and returns CHROMOSOME."
  (let ((track-result (main :ga-piece-data (gene c) :track track)))
    (if (or (null track-result)
            (equal :crash track-result))
        (setf (fitness c) 3600001)
        (setf (fitness c) track-result)))
  c)


(defmethod mate ((c1 chromosome) (c2 chromosome))
  "Method used to mate the chromosome C1 with C2 resulting in a list
  containing two new chromosomes."
  (let* ((pivot (random (length (gene c1))))
         (gene1 (concatenate 'vector (subseq (gene c1) 0 pivot)
                                     (subseq (gene c2) pivot)))
         (gene2 (concatenate 'vector (subseq (gene c2) 0 pivot)
                                     (subseq (gene c1) pivot))))
    (list (make-chromosome nil nil nil gene1)
          (make-chromosome nil nil nil gene2))))


(defmethod mutate ((c chromosome))
  "Method used to generate a new chromosome based on a change in a
  the gene of this chromosome.
  Returns a new chromosome instance."
  (let* ((gene (loop for piece across (gene c)
                     collect (loop for lane across piece
                                   collect (copy-seq lane) into lanes
                                   finally (return (coerce lanes 'vector)))
                       into result
                     finally (return (coerce result 'vector))))
         (random-index (random (length gene)))
         (lanes (length (elt gene 0)))
         (delta-lane (random lanes))
         (delta-speed (let ((speed (getf (elt (elt gene random-index)
                                              delta-lane)
                                         :max-speed)))
                        (- (random (/ speed 4.0)) (/ speed 8.0)))))
    (when (< delta-speed 0.1)
      (setf delta-speed 0.1))
    (setf (getf (elt (elt gene random-index) delta-lane) :max-speed)
          delta-speed)
    (make-chromosome nil nil nil gene)))


(defmethod print-object ((obj chromosome) stream)
  (print-unreadable-object (obj stream :type t)
    (format stream "f=~,3Fs" (/ (fitness obj) 1000))))


;;; Population Methods

(defmethod evolve ((p population) track)
  "Method to evolve the population of chromosomes."
  (loop with index = (floor (* (size p) (elitism p)))
        with new-chromosomes = (subseq (chromosomes p) 0 index)
        while (< index (size p))
        do (if (<= (random 1.0) (crossover p))
               (let ((children (mate (tournament-selection p)
                                     (tournament-selection p))))
                 (loop for child in children
                       do (if (<= (random 1.0) (mutation p))
                              (push (set-fitness (mutate child) track)
                                    new-chromosomes)
                              (push (set-fitness child track)
                                    new-chromosomes))
                          (sleep 2))
                 (incf index 2))
               (progn
                 (if (<= (random 1.0) (mutation p))
                     (push (set-fitness (mutate (elt (chromosomes p) index))
                                        track)
                           new-chromosomes)
                     (push (set-fitness (elt (chromosomes p) index) track)
                           new-chromosomes))
                 (incf index)))
           (sleep 2)
        finally (setf (chromosomes p)
                      (sort new-chromosomes (lambda (a b)
                                              (< (fitness a) (fitness b)))))))


(defmethod print-object ((obj population) stream)
  (print-unreadable-object (obj stream :type t)
    (format stream "~D ~S" (size obj) (elt (chromosomes obj) 0))))


(defmethod tournament-selection ((p population))
  (loop with best = (random (size p))
        repeat (tournament-size p)
        for cont = (random (size p))
        ;; We're comparing the indexes directly since the chromosomes are
        ;; sorted from best to worst.  Robust code should compare the fitness
        ;; of the candidates.
        when (< cont best) do (setf best cont)
        finally (return (elt (chromosomes p) best))))


(defmethod write-population ((population population) &optional
                             (file "populations/population.cl-store"))
  (cl-store:store population file))


;;; Main Program

(defun ga-main (&key (generations 1) population (size 32) (track "keimola"))
  (setf *random-state* (make-random-state t))
  (let ((*running-in-ga* t)
        (p (if population
               population
               (make-population track :size size :crossover 0.8 :elitism 0.1
                                :mutation 0.3))))
    (loop for g from 0 below generations
          for best-gene = (first (chromosomes p))
          for fitness = (fitness best-gene)
          do (format t "Generation ~D: ~S~%" g best-gene)
             (evolve p track)
             (write-population p (mkstr "populations/" track "-" size
                                        ".cl-store"))
             (sleep 5))
    p))
